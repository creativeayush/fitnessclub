import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'signin', loadChildren: './login/signin/signin.module#SigninPageModule' },
  { path: 'signup', loadChildren: './login/signup/signup.module#SignupPageModule' },
  { path: 'landing', loadChildren: './landing/landing.module#LandingPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'dietplan', loadChildren: './landing/dietplan/dietplan.module#DietplanPageModule' },
  { path: 'track', loadChildren: './landing/track/track.module#TrackPageModule' },
  { path: 'workoutgenerater', loadChildren: './landing/workoutgenerater/workoutgenerater.module#WorkoutgeneraterPageModule' },
  { path: 'ourplan', loadChildren: './landing/ourplan/ourplan.module#OurplanPageModule' },
  { path: 'setting', loadChildren: './setting/setting.module#SettingPageModule' },
  { path: 'notification', loadChildren: './notification/notification.module#NotificationPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    HttpClientModule
  ],
  exports: [RouterModule],
  providers: [
]
})
export class AppRoutingModule { }
