import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DietplanPage } from './dietplan.page';

describe('DietplanPage', () => {
  let component: DietplanPage;
  let fixture: ComponentFixture<DietplanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DietplanPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DietplanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
