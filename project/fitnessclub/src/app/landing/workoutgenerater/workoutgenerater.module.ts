import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { WorkoutgeneraterPage } from './workoutgenerater.page';

const routes: Routes = [
  {
    path: '',
    component: WorkoutgeneraterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [WorkoutgeneraterPage]
})
export class WorkoutgeneraterPageModule {}
